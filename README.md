This is a skin for the DJ program Mixxx made for a 1680 x 1050 resolution.

![Skin preview](preview.png "Skin preview")

You can use it by cloning this folder in your Mixxx skin directory. On Debian:

    $ cd /usr/share/mixxx/skins/
    $ sudo git clone https://gitlab.com/baldurmen/mixxx_radio_king_skin.git

If Mixxx is already open, you will need to restart it for the skin to appear in
the Interface menu.

You will also need to install the [Titillium font][titillium] on your machine.

[titillium]: https://www.fontsquirrel.com/fonts/Titillium

## Authors

* Tristan Lippens (99,9% of the work)
* Louis-Philippe Véronneau (very minor modifications)
